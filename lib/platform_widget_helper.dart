import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlatformWidgetHelper {
  static Widget getAppWidget(BuildContext context, Widget homeWidget) {
    String title = 'Flutter Demo';

    if (Platform.isIOS) {
      return CupertinoApp(
        title: title,
        home: homeWidget,
        theme: CupertinoThemeData(
          primaryColor: CupertinoColors.activeBlue,
        ),
      );
    } else {
      return MaterialApp(
        title: title,
        home: homeWidget,
        theme: ThemeData(
          primaryColor: Colors.blue,
        ),
      );
    }
  }

  static Widget getButton({
    @required String text,
    @required Function onPressed,
  }) {
    if (Platform.isIOS) {
      return CupertinoButton.filled(
        child: Text(text),
        onPressed: () => onPressed(),
      );
    } else {
      return RaisedButton(
        child: Text(text),
        onPressed: () => onPressed(),
      );
    }
  }

  static Widget getTextButton({
    @required String text,
    @required Function onPressed,
  }) {
    if (Platform.isIOS) {
      return CupertinoButton(
        child: Text(text),
        onPressed: () => onPressed(),
      );
    } else {
      return FlatButton(
        child: Text(text),
        onPressed: () => onPressed(),
      );
    }
  }

  static Widget getScaffold(
      {Color backgroundColor,
      @required ObstructingPreferredSizeWidget cupertinoNavigationBar,
      @required AppBar materialAppBar,
      @required Widget child,
      bool resizeToAvoidBottomInset = true}) {
    if (Platform.isIOS) {
      return CupertinoPageScaffold(
        child: Material(child: child),
        backgroundColor: backgroundColor,
        navigationBar: cupertinoNavigationBar,
        resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      );
    } else {
      return Scaffold(
        body: child,
        backgroundColor: backgroundColor,
        appBar: materialAppBar,
      );
    }
  }

  static Widget getClickableIcon({
    @required IconData iconDataIOS,
    @required IconData iconDataAndroid,
    @required Function onTap,
  }) {
    if (Platform.isIOS) {
      return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: onTap,
        child: Icon(
          iconDataIOS,
          size: 24,
        ),
      );
    } else {
      return IconButton(
        icon: Icon(iconDataAndroid),
        onPressed: () => onTap,
      );
    }
  }

  static Widget getLoadingWidget() {
    if (Platform.isIOS) {
      return CupertinoActivityIndicator();
    } else {
      return CircularProgressIndicator();
    }
  }

  static Widget getTextField(
      {bool autoFocus = false,
      TextEditingController controller,
      int maxLines,
      TextCapitalization textCapitalization = TextCapitalization.none,
      TextInputAction textInputAction = TextInputAction.done,
      BoxDecoration decorationIOS,
      InputDecoration decorationAndroid,
      ValueSetter<String> onChanged,
      //iOS
      String placeholder}) {
    if (Platform.isIOS) {
      return CupertinoTextField(
        textInputAction: textInputAction,
        autofocus: autoFocus,
        controller: controller,
        maxLines: maxLines,
        textCapitalization: textCapitalization,
        decoration: decorationIOS,
        placeholder: placeholder,
        onChanged: (input) => onChanged(input),
      );
    } else {
      return TextFormField(
        textInputAction: textInputAction,
        autofocus: autoFocus,
        controller: controller,
        maxLines: maxLines,
        textCapitalization: textCapitalization,
        decoration: decorationAndroid,
        onChanged: (input) => onChanged(input),
      );
    }
  }
}
