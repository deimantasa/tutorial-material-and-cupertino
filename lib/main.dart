import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tutorialmaterialcupertinoui/platform_widget_helper.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PlatformWidgetHelper.getAppWidget(context, HomePage());
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleWidget = Text("Home Page");
    String hintText = "Text Form Field Hint";
    Widget leadingIcon = PlatformWidgetHelper.getClickableIcon(
      iconDataIOS: CupertinoIcons.minus_circled,
      iconDataAndroid: Icons.remove_circle_outline,
      onTap: () {},
    );
    Widget trailingIcon = PlatformWidgetHelper.getClickableIcon(
      iconDataIOS: CupertinoIcons.plus_circled,
      iconDataAndroid: Icons.add_circle_outline,
      onTap: () {},
    );

    return PlatformWidgetHelper.getScaffold(
      cupertinoNavigationBar: CupertinoNavigationBar(
        leading: leadingIcon,
        middle: titleWidget,
        trailing: trailingIcon,
      ),
      materialAppBar: AppBar(
        leading: leadingIcon,
        title: titleWidget,
        actions: [
          trailingIcon,
        ],
      ),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            shrinkWrap: true,
            children: [
              PlatformWidgetHelper.getButton(text: "Raised Button", onPressed: () {}),
              SizedBox(
                height: 8,
              ),
              PlatformWidgetHelper.getTextButton(text: "Flat Button", onPressed: () {}),
              SizedBox(
                height: 8,
              ),
              PlatformWidgetHelper.getTextField(
                decorationAndroid: InputDecoration(hintText: hintText),
                placeholder: hintText,
              ),
              SizedBox(
                height: 8,
              ),
              Container(width: 48, height: 48, child: Center(child: PlatformWidgetHelper.getLoadingWidget())),
            ],
          ),
        ),
      ),
    );
  }
}
